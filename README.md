[o4u](http://www.o4u.co.in/)
--- 


### Install dependencies and libraries
```shell
sudo apt-get update
sudo apt-get -y install build-essential mysql-server libmysqlclient-dev rabbitmq-server git uwsgi nginx htop
sudo apt-get -y install python3-dev python3-pip uwsgi-plugin-python3
```

### Clone Repo in /srv directory
```shell
sudo chown -R `whoami` /srv
sudo chgrp -R `whoami` /srv
cd /srv
git clone https://github.com/devzop/DarkLord.git

sudo pip3 install -r darklord/src/requirements.txt
```

### MySQL database setup
```sh
mysql -u root -p
Enter password:
CREATE DATABASE darklord character set utf8 collate utf8_general_ci;
CREATE USER 'gaurav'@'localhost' IDENTIFIED BY 'z0pp3r';
GRANT ALL PRIVILEGES ON darklord.* TO gaurav@localhost;
FLUSH PRIVILEGES;
exit
```

### RabbitMQ server setup (Message Broker for celery)
```sh
sudo rabbitmqctl add_user gaurav gaurav
sudo rabbitmqctl add_vhost darklord
sudo rabbitmqctl set_permissions -p darklord gaurav ".*" ".*" ".*"

# Enable rabbitmq admin/management panel
sudo rabbitmq-plugins enable rabbitmq_management
sudo service rabbitmq-server restart
# sudo rabbitmqctl list_users  # list all users
sudo rabbitmqctl set_user_tags gaurav administrator
# visit http://localhost:15672/      username/password=gaurav/gaurav
```

### Create darklord/settings/dev.py file  (For local system ~ Dev mode)

```python
TEMPLATE_DEBUG = DEBUG = True
ALLOWED_HOSTS = [] if DEBUG else ['*']
```

### Running the project First time 

```sh
python3 manage.py makemigrations stores
python3 manage.py migrate
# python3 manage.py migrate djcelery
python3 manage.py createsuperuser
python3 manage.py collectstatic  # for production 
#python3 manage.py runserver

# python3 manage.py celeryd -B -l DEBUG #depricated
# python3 manage.py celery worker -l DEBUG
```

### create directory 'files' with 777 permission
```sh
cd /srv/darklord/src/darklord
mkdir files
cd files

mkdir general
mkdir general/product_mapping general/store_info general/upload_brands general/upload_category_tags

mkdir prestige
mkdir prestige/product_info

mkdir tms
mkdir tms/product_info

cd ..
chmod -R 777 files


#general
#│   ├── product_mapping
#│   ├── store_info
#│   ├── upload_brands
#│   └── upload_category_tags
#├── prestige
#│   └── product_info
#└── tms
#    └── product_info

```
