import json
import logging
from datetime import datetime

from annoying.functions import get_object_or_None
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.template.defaultfilters import slugify

from user_info.models import VendorUser
from catalog.models import Services, SubService, ServicePic, Pricing, SubServicePrice
from catalog.views.home import get_occasion, get_outing, get_offer, displayprice

from project_O import settings

logging = logging.getLogger(__name__)


@csrf_exempt
def authenticate_vendor(request):
    response_data = {}
    data = json.loads(str(request.body.decode('utf8')))
    email = data['email']
    password = data['password']
    response_data['result'] = "error"

    if email and password:
        custom_base_user_obj = authenticate(email=email, password=password)
        if custom_base_user_obj:
            if custom_base_user_obj.is_vendor and custom_base_user_obj.is_active and custom_base_user_obj.is_staff:
                response_data['result'] = "Success"
                response_data['message'] = "Success"
                logging.info("Authenticated successfully for User %s" % email)
                login(request, custom_base_user_obj)
                return HttpResponse(json.dumps(response_data), status=200)

            else:
                response_data['message'] = "Incorrect Password"
                logging.info("Incorrect User password for %s" % email)
        else:
            response_data['message'] = "This Email is not registered with us"
            logging.info("Incorrect User Email %s" % email)
    return HttpResponse(json.dumps(response_data), status=200)


def login_vendor(request, template="login.html"):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()

    if request.method == 'POST':
        email = (request.POST.get('email', False))
        password = (request.POST.get('password', False))
        response_data = authenticate_vendor(email, password)
        return HttpResponse(json.dumps(response_data), status=200)

    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list}
    return render(request, template, data)


@csrf_exempt
@login_required(login_url="/vendor/login")
def logout_vendor(request):
    if request.method == 'GET':
        request.session.flush()
        logout(request)
        return redirect('/vendor/login')
    else:
        return HttpResponse("Failure", status=404)


@login_required(login_url="/vendor/login")
def vendor_home(request, template="vendor_view_offers.html"):
    data = {}
    sub_service_list = []
    # todo remove hardcode
    rating_count = 3
    vendor_name = request.user.first_name
    vendor_user_obj = get_object_or_None(VendorUser, custom_base_user=request.user)
    if vendor_user_obj:
        sub_service_objs = SubService.objects.filter(service__in=Services.objects.filter(vendor_user=vendor_user_obj),)
        for sub_service in sub_service_objs:
            service_pic_obj = ServicePic.objects.filter(sub_service=sub_service)
            jsondata = {}
            jsondata['status'] = sub_service.sub_service_status_vendor
            if len(service_pic_obj) > 0 and service_pic_obj[0].image:
                jsondata['image'] = service_pic_obj[0].image.url
            else:
                jsondata['image'] = settings.DEFAULT_IMAGE

            pricingset = sub_service.pricing_set.all()
            jsondata['id'] = sub_service.pk
            jsondata['name'] = sub_service.name
            jsondata['slug'] = sub_service.slug
            jsondata['price'] = []
            for pricing in pricingset:
                p = {'type': pricing.price_type, 'mrp': pricing.final_amount(),
                     'discounted_price': pricing.get_discounted_price(), 'displayprice': displayprice(pricing)}
                jsondata['price'].append(p)

            sub_service_list.append(jsondata)

    data = {'vendor_name': vendor_name, 'sub_services': sub_service_list}
    return render(request, template, data)


@csrf_exempt
@login_required(login_url="/vendor/login")
def vendor_del_sub_service(request):
    if request.method == 'POST':
        data = request.body
        received_data = json.loads(data.decode("ascii", "ignore"))
        sub_service_id = received_data.get('sub_service_id', None)

        if sub_service_id:
            sub_service_obj = get_object_or_None(SubService, pk=sub_service_id)
            if sub_service_obj:
                sub_service_obj.is_active = False
                sub_service_obj.save()
                return HttpResponse("Success", status=200)
        return HttpResponse("Failure", status=200)
    else:
        return HttpResponse("Get Method not supported")


@csrf_exempt
@login_required(login_url="/vendor/login")
def edit_sub_service(request, sub_service_id, template="vendor_offer_edit.html"):
    if request.method == 'GET':
        vendor_name = request.user.first_name

        sub_service_obj = get_object_or_None(SubService, pk=sub_service_id)
        if sub_service_obj:
            name = sub_service_obj.name
            offer = sub_service_obj.gist
            text_message = sub_service_obj.description

            pricing_obj = sub_service_obj.pricing_set.all()[0]
            min_bill_amount = pricing_obj.discount

            from_duration = ''
            to_duration = ''
            if sub_service_obj.from_duration:
                from_duration = sub_service_obj.from_duration.strftime("%m/%d/%Y")
            if sub_service_obj.to_duration:
                to_duration = sub_service_obj.to_duration.strftime("%m/%d/%Y")

        data = {'vendor_name': vendor_name, 'id': sub_service_id, 'name': name, 'offer': offer,
                'text_message': text_message,
                'min_bill_amount': min_bill_amount, 'from_duration': from_duration, 'to_duration': to_duration}
        return render(request, template, data)


@csrf_exempt
@login_required(login_url="/vendor/login")
def edit_sub_service_update(request, sub_service_id):
    if request.method == 'POST':
        data = request.body
        received_data = json.loads(data.decode("ascii", "ignore"))
        sub_service_obj = get_object_or_None(SubService, pk=sub_service_id)
        if sub_service_obj:
            sub_service_obj.name = received_data.get('name', None)
            sub_service_obj.slug = slugify(received_data.get('name') + " " + str(sub_service_obj.service.pk))
            sub_service_obj.gist = received_data.get('gist')
            min_bill_amount = received_data.get('min_bill_amount')
            from_duration = received_data.get('from_duration')
            to_duration = received_data.get('to_duration')
            sub_service_obj.description = received_data.get('text_message')
            sub_service_obj.from_duration = datetime.strptime(from_duration, "%m/%d/%Y")
            sub_service_obj.to_duration = datetime.strptime(to_duration, "%m/%d/%Y")
            sub_service_obj.is_active = False
            sub_service_obj.save()

            pricing_obj = sub_service_obj.pricing_set.all()[0]
            pricing_obj.discount = min_bill_amount
            pricing_obj.save()
            return HttpResponse("Success", status=200)
        return HttpResponse("Failure", status=200)
    else:
        return HttpResponse("Get Method not supported")


@csrf_exempt
@login_required(login_url="/vendor/login")
def create_sub_service(request, template="vendor_create_offer.html"):
    vendor_name = request.user.first_name
    if request.method == 'POST':
        data = request.body
        recieved_data = json.loads(data.decode("ascii", "ignore"))
        name = recieved_data.get('offer_name', None)
        gist = recieved_data.get('offer', None)
        min_bill_amount = recieved_data.get('min_bill_amount', None)
        from_duration = recieved_data.get('from_duration', None)
        to_duration = recieved_data.get('to_duration', None)
        description = recieved_data.get('text_message', None)

        from_duration = datetime.strptime(from_duration, "%m/%d/%Y")
        to_duration = datetime.strptime(to_duration, "%m/%d/%Y")

        vendor_user_obj = get_object_or_None(VendorUser, custom_base_user=request.user)
        try:
            if vendor_user_obj:
                services_obj = Services.objects.filter(vendor_user=vendor_user_obj)
                if services_obj:
                    slug = slugify(name + " " + str(services_obj[0].pk))
                    sub_service_obj = SubService(service=services_obj[0], slug=slug, name=name, gist=gist,
                                                 description=description, from_duration=from_duration,
                                                 to_duration=to_duration, is_active=False)
                    pricing_obj = Pricing(name=name, price_type='% off on total bill amount', discount=min_bill_amount)
                    sub_service_obj.save()
                    pricing_obj.save()
                    sub_service_price_obj = SubServicePrice(pricing=pricing_obj, subservice=sub_service_obj)
                    sub_service_price_obj.save()
                return HttpResponse("Success", status=200)
        except Exception as e:
            print(str(e))
            return HttpResponse("Failure", status=200)
    return render(request, template, {'vendor_name': vendor_name})
