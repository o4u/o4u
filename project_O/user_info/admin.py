from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomBaseUser, VendorUser, Vendor, Location
from user_info.admin_forms import UserCreationForm


class CustomUserAdmin(UserAdmin):
    add_form = UserCreationForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        # ('Personal info', {'fields': ('first_name', 'last_name', 'phone', 'title')}),
        # ('Company', {'fields': ('company', 'my_cart')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'is_vendor', 'phone_no')}),
        # ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_email_verified', 'groups', 'user_permissions')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_email_verified')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )

    list_display = ('email', 'is_vendor', 'is_superuser')
    list_filter = ('is_active', 'is_email_verified')
    search_fields = ('first_name', 'last_name', 'email', 'is_vendor')
    ordering = ('email',)


class VendorAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'description', 'logo_info', 'average_rating' )
    list_filter = ('name', 'view_count')
    search_fields = ('name', 'view_count')
    # readonly_fields = ('logo_info',)
    prepopulated_fields = {'slug': ('name',)}


class VendorUserAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'description', 'image_info', 'view_count' )
    prepopulated_fields = {'slug': ('name','view_count')}
    # readonly_fields = ('image_info',)


class LocationAdmin(admin.ModelAdmin):
    list_display = ('state', 'city', 'area', 'latitude', 'longitude')
    list_filter = ('state', 'city', 'area')
    search_fields = ('state', 'city', 'area')


admin.site.register(Location, LocationAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(CustomBaseUser, CustomUserAdmin)
admin.site.register(VendorUser, VendorUserAdmin)


