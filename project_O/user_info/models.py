import datetime
import logging

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.http import urlquote

logging = logging.getLogger(__name__)


STATE = (
    ('Karnataka', 'Karnataka'),
)

CITY = (
    ('Bangalore', 'Bangalore'),
)


class BaseModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('The given email address must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=False, is_active=True, is_superuser=False,
                          last_login=now, date_joined=now, **extra_fields)

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        u = self.create_user(email, password, **extra_fields)
        u.is_staff = True
        u.is_active = True
        u.is_superuser = True
        u.save(using=self._db)
        return u


class CustomBaseUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_email_verified = models.BooleanField('verified', default=False)
    date_joined = models.DateTimeField(default=timezone.now)
    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    is_vendor = models.BooleanField(default=False)
    phone_no = models.CharField(max_length=16, blank=True, null=True, default='')

    objects = CustomUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.email)

    def natural_key(self):
        return self.email

    def get_short_name(self):
        return self.email

    def get_full_name(self):
        return self.email

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        # logging.info("CustomBaseUser:: calling save method ")
        super(CustomBaseUser, self).save()


class Location(BaseModel):
    state = models.CharField(max_length=255, choices=STATE, blank=True, null=True)
    city = models.CharField(max_length=255, choices=CITY, blank=True, null=True)
    area = models.CharField(max_length=255, blank=False, null=False)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    class Meta:
        verbose_name = 'Location'
        verbose_name_plural = 'Locations'
        unique_together = (('state', 'city', 'area'),)

    def __str__(self):
        return "%s %s %s" % (self.state, self.city, self.area)


class Vendor(BaseModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)

    gist = models.CharField(
        max_length=500, null=True, blank=True, help_text='Short description of the Vendor')
    description = models.TextField(
        null=True, blank=True, help_text='Full description of the Vendor')

    logo = models.ImageField(upload_to='images/vendor/logo', null=True, blank=True)

    tags = models.CharField(max_length=100, null=True, blank=True,
                            help_text='Comma-delimited set of SEO keywords for meta tag')

    mobile_nos = models.CharField(max_length=1023, blank=True, null=True)
    phone_nos = models.CharField(max_length=1023, blank=True, null=True)

    average_rating = models.FloatField(default=0.0, blank=True, null=True)
    rating_count = models.IntegerField(default=0, blank=True, null=True)
    view_count = models.IntegerField(default=0, blank=True, null=True)

    def logo_info(self):
        if self.logo:
            return '<img src="%s" style="height: 50px;max-width: auto"/>' % self.logo.url
        else:
            return 'Image not found'

    logo_info.allow_tags = True

    class Meta:
        verbose_name = 'Vendor'
        verbose_name_plural = 'Vendors'

    def __str__(self):
        return "%s  %s" % (self.name, self.mobile_nos)


class VendorUser(BaseModel):
    vendor = models.ForeignKey(Vendor, related_name='vendor_user')
    custom_base_user = models.OneToOneField(CustomBaseUser, on_delete=models.SET_NULL, null=True, blank=True)

    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)

    gist = models.CharField(
        max_length=500, null=True, blank=True, help_text='Short description of the VendorUser')
    description = models.TextField(
        null=True, blank=True, help_text='Full description of the VendorUser')

    image = models.ImageField(upload_to='images/vendor/image', null=True, blank=True)

    mobile_nos = models.CharField(max_length=1023, blank=True, null=True)
    phone_nos = models.CharField(max_length=1023, blank=True, null=True)
    address = models.CharField(max_length=1023, blank=True, null=True)

    landmark = models.CharField(max_length=1023, blank=True, null=True)
    locality = models.CharField(max_length=1023, blank=True, null=True)
    location = models.ForeignKey(Location)
    pincode = models.CharField(max_length=20, blank=True, null=True)

    open_time = models.TimeField(default=datetime.datetime(2015, 1, 1, 9, 00, 00))
    close_time = models.TimeField(default=datetime.datetime(2015, 1, 1, 21, 00, 00))

    virtual_nos = models.CharField(max_length=1023, blank=True, null=True)

    average_rating = models.FloatField(default=0.0, blank=True, null=True)
    rating_count = models.IntegerField(default=0, blank=True, null=True)
    view_count = models.IntegerField(default=0, blank=True, null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'VendorUser'
        verbose_name_plural = 'VendorUsers'

    def image_info(self):
        if self.image:
            return '<img src="%s" style="height: 50px;max-width: auto"/>' % self.image.url
        else:
            return 'Image not found'

    image_info.allow_tags = True

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        logging.info("Vendor :: calling save method ")
        super(VendorUser, self).save()

