from django.conf.urls import include, url

urlpatterns = [
    url(r'^authenticate_vendor', 'user_info.views.authenticate_vendor', name='authenticate_vendor'),
    url(r'^login', 'user_info.views.login_vendor', name="vendor-login"),
    url(r'^logout', 'user_info.views.logout_vendor', name="vendor-logout"),
    url(r'^home', 'user_info.views.vendor_home', name="vendor-home"),
    url(r'^edit_sub_service/(?P<sub_service_id>[\w]+$)', 'user_info.views.edit_sub_service', name="edit-sub-service"),
    url(r'^edit_sub_service_update/(?P<sub_service_id>[\w]+$)', 'user_info.views.edit_sub_service_update',
        name="edit-sub-service-update"),
    url(r'^del_sub_service$', 'user_info.views.vendor_del_sub_service', name="del_sub_service"),
    url(r'^create_sub_service', 'user_info.views.create_sub_service', name="create-sub-service"),



]
