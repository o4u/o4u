from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls import handler404,handler500
from django.views.generic import TemplateView

urlpatterns = [
    url(r'^vrlib/', TemplateView.as_view(template_name="vr.htm"), name='vr'),
    url(r'^virtual-tour/(?P<ssname>[-\w]+)/$','catalog.views.home.virtual_tour', name='virtual_tour'),

    url(r'^admin/', include(admin.site.urls)),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT.replace('\\', '/')},name='media'),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_PATH},name='static'),

    url(r'^sales/', include('sales.urls', namespace='sales')),
    url(r'^$', 'catalog.views.home.home', name="home"),


    url(r'^(?P<slug1>[-\w]+)/(?P<slug2>[-\w]+)/(?P<slug3>[-\w]+)/(?P<ssname>[-\w]+)',
        'catalog.views.home.third_screen', name='catalog_category'),

    url(r'^(?P<slug1>[-\w]+)/(?P<slug2>[-\w]+)/(?P<slug3>[-\w]+)/$',
        'catalog.views.home.second_screen', name='catalog_category'),

    url(r'^(?P<slug1>[-\w]+)/(?P<slug2>[-\w]+)/$',
        'catalog.views.home.second_screen', name='catalog_category'),

    url(r'^catalog/', include('catalog.urls', namespace='catalog')),
    url(r'^vendor/', include('user_info.urls', namespace='user_info', app_name='UserInfo')),
    url(r'^faq/','catalog.views.footer.faq', name='faq'),
    url(r'^contact_us/','catalog.views.footer.contact_us', name='contact_us'),
    url(r'^about_us/','catalog.views.footer.about_us', name='about_us'),
    url(r'^terms-condition/','catalog.views.footer.terms', name='terms_condition'),
    # url(r'^vendor/del_sub_service', 'user_info.views.vendor_del_sub_service', name="del_sub_service"),


]
handler404 = 'catalog.views.home.handler404'
handler500 = 'catalog.views.home.handler500'

