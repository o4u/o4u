import json
import datetime

from django.db import models

from user_info.models import BaseModel, VendorUser, CustomBaseUser

DATA_TYPES = (
    (0, 'INT'),
    (1, 'CHAR'),
    (3, 'BOOLEAN'),
)


class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value':self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


class Category(BaseModel):
    """
    Represents a All Categories
    """
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)

    gist = models.CharField(
        max_length=500, null=True, blank=True, help_text='Short description of the category')
    description = models.TextField(
        null=True, blank=True, help_text='Full description of the category')

    image = models.ImageField(upload_to='images/catalog/category/image', null=True, blank=True)
    logo = models.ImageField(upload_to='images/catalog/category/logo', null=True, blank=True)
    icon = models.ImageField(upload_to='images/catalog/category/icon', null=True, blank=True)

    parent = models.ForeignKey('self', related_name='sub_categories', null=True, blank=True)
    tags = models.TextField(null=True, blank=True,
                            help_text='Comma-delimited set of SEO keywords for meta tag')

    display_order = models.IntegerField(default=1)
    is_active = models.BooleanField(default=True)

    class Meta:
        ordering = ('display_order', )
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        if self.parent:
            return '%s > %s' % (self.parent, self.name)

        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('catalog_category', [self.slug,])

    def image_info(self):
        if self.image:
            return '<img src="%s" style="height: 50px;max-width: auto"/>' % self.image.url
        else:
            return 'Image not found'
    image_info.allow_tags = True

    def logo_info(self):
        if self.logo:
            return '<img src="%s" style="height: 50px;max-width: auto"/>' % self.logo.url
        else:
            return 'Image not found'
    logo_info.allow_tags = True


class CategoryBanners(BaseModel):

    category = models.ForeignKey(Category, help_text='Category')
    banner = models.ImageField(upload_to='images/catalog/category/banner', null=False, blank=False)
    text = models.TextField(default='', blank=True, null=True)
    display_order = models.IntegerField(default=1)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return '%s [Pic #id %s]' % (self.category, self.id)

    class Meta:
        ordering = ('display_order', )
        verbose_name = 'Category Banners'
        verbose_name_plural = 'Category Banners'

    def banner_info(self):
        if self.banner:
            return '<img src="%s" style="height: 50px;max-width: auto"/>' % self.banner.url
        else:
            return 'Image not found'
    banner_info.allow_tags = True


class Services(BaseModel):
    """
    Represents a Service
    """
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)

    vendor_user = models.ForeignKey(VendorUser, help_text='Vendor User')
    category = models.ManyToManyField(Category, help_text='Multiple Categories', related_name='service')

    is_active = models.BooleanField(
        default=True, help_text='Service is available for listing and sale')

    tags = models.CharField(max_length=250, null=True, blank=True,
                            help_text='Comma-delimited set of SEO keywords for meta tag')

    class Meta:
        ordering = ('id',)
        verbose_name = 'Services'
        verbose_name_plural = 'Services'

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return reverse('catalog_services', [self.slug, ])


class SubService(BaseModel):
    """
    Represents a SubService
    """
    service = models.ForeignKey(Services, related_name='sub_service')
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    mrp = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    discounted_price = models.DecimalField(decimal_places=2, max_digits=10, default=0)

    gist = models.TextField(
        null=True, blank=True, help_text='Short description/Quick Overview of the service')
    description = models.TextField(
        null=True, blank=True, help_text='Full description displayed on the service page')

    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    # quantity = models.IntegerField(blank=True, null=True, default=0)

    is_active = models.BooleanField(
        default=True, help_text='Service is available for listing and sale')
    is_bestseller = models.BooleanField(
        default=False, help_text='It has been best seller')
    is_featured = models.BooleanField(
        default=False, help_text='Promote this service on main pages')
    is_new = models.BooleanField(
        default=False, help_text='Promote this New service')
    display_order = models.IntegerField(default=1)

    tags = models.CharField(max_length=250, null=True, blank=True,
                            help_text='Comma-delimited set of SEO keywords for meta tag')

    virtual_tour_url = models.CharField(max_length=250, null=True, blank=True)
    # for offers
    min_bill_amount = models.DecimalField(default=0, decimal_places=2, max_digits=10)
    from_duration = models.DateField(default=datetime.datetime.now)
    to_duration = models.DateField(default=datetime.datetime.now)

    avg_rating = models.IntegerField(default=3)

    class Meta:
        ordering = ('display_order', 'id',)
        verbose_name = 'Sub Service'
        verbose_name_plural = 'Sub Services'

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return 'catalog_subservice', [self.slug, ]

    @property
    def sub_service_status_vendor(self):
        if self.is_active is True:
            return "Active"
        else:
            return "Deactivated"


class Review(BaseModel):
    """
    Represents the reviews written for a sub service
    """
    reviewed_item = models.ForeignKey(SubService, related_name='review_service')
    user = models.ForeignKey(VendorUser, null=True, blank=True, related_name='review_user', default='')
    price_rating = models.IntegerField(default=3)
    quality_rating = models.IntegerField(default=3)
    is_verified = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    nickname = models.CharField(max_length=255, blank=True, null=True)
    summary = models.CharField(max_length=255, blank=True, null=True)
    comment = models.TextField(null=True, blank=True, help_text='Your Review')

    def calculate_average_rating(reviews):
        sum_of_price_ratings = 0
        sum_of_quality_ratings = 0
        for obj in reviews:
            sum_of_price_ratings += obj.price_rating
            sum_of_quality_ratings += obj.quality_rating
        return (float(sum_of_price_ratings)/len(reviews) + float(sum_of_quality_ratings)/len(reviews))/2


class SpecName(models.Model):
    name = models.CharField(max_length=255, blank=False, null=False)
    data_type = models.IntegerField(choices=DATA_TYPES, default=1, blank=True, null=True)

    def __str__(self):
        return '%s' % self.name

    class Meta:
        verbose_name = 'Spec Name'
        verbose_name_plural = 'Spec Names'


class ServiceSpec(BaseModel):
    """
    Represents service specification attribute
    """
    service = models.ForeignKey(SubService, related_name='service_spec')
    name = models.ForeignKey(SpecName)
    value = models.CharField(max_length=250)
    display_order = models.IntegerField(default=0)
    is_filterable = models.BooleanField(default=False)

    class Meta:
        ordering = ('display_order', 'id',)
        unique_together = ('service', 'name',)
        verbose_name = 'Service Spec'
        verbose_name_plural = 'Service Specs'

    def __str__(self):
        return '%s: %s' % (self.name, self.value)


class ServicePic(BaseModel):
    """
    Represents service picture
    """
    sub_service = models.ForeignKey(SubService, related_name='service_pic')

    image = models.ImageField(upload_to="images/catalog/service", null=False, blank=False)
    display_order = models.IntegerField(default=0)

    class Meta:
        ordering = ('display_order', 'id')
        verbose_name = 'Service Pic'
        verbose_name_plural = 'Service Pics'

    def __str__(self):
        return u'%s' % self.image
        # return '%s [Pic #id %s]' % (self.product, self.id)

    def admin_url(self):
        return '<img src="%s"/>' % self.image
    admin_url.allow_tags = True


class Home(BaseModel):
    IMAGE_TYPE = (
        ('Banner', 'Banner'),
        ('Promotions', 'Promotions'),
        ('Offers', 'Offers'),
    )

    image_type = models.CharField(max_length=100, choices=IMAGE_TYPE, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    description = models.TextField(null=True, blank=True, help_text='description of Home Page')
    alt = models.CharField(max_length=255, blank=True, null=True)
    image = models.ImageField(upload_to='images/home', null=True, blank=True)
    header_text = models.TextField(null=True, blank=True)
    middle_text = models.TextField(null=True, blank=True)
    footer_text =  models.TextField(null=True, blank=True)

    action_url = models.URLField(max_length=500,null=True, blank=True)

    button1_image = models.ImageField(upload_to='images/home', null=True, blank=True)
    button1_text = models.CharField(max_length=255, blank=True, null=True)
    button1_action_url = models.URLField(max_length=500,null=True, blank=True)

    button2_text = models.CharField(max_length=255, blank=True, null=True)
    button2_image = models.ImageField(upload_to='images/home', null=True, blank=True)
    button2_action_url = models.URLField(max_length=500,null=True, blank=True)

    is_active = models.BooleanField(default=False)
    version = models.IntegerField(default=0, blank=True, null=True)
    display_order = models.IntegerField(default=1)


    class Meta:
        ordering = ('display_order', )
        verbose_name = 'Home'
        verbose_name_plural = 'Home'

    def image_info(self):
        if self.image:
            return '<img src="%s" style="height: 50px;max-width: auto"/>' % self.image.url
        else:
            return 'Image not found'
    image_info.allow_tags = True


class HomeFooter(models.Model):
    client_list = models.CharField(max_length=200, blank=True, null=True)
    contact_us = models.TextField(null=True, blank=True)
    terms_condtions = models.TextField(null=True, blank=True) #site map, search terms
    about_us = models.TextField(null=True, blank=True)
    faq_question = models.TextField(null=True, blank=True)
    faq_answer = models.TextField(null=True, blank=True)

    def set_client_list(self, client_list):
        self.client_list = json.dumps(client_list)

    def get_client_list(self, client_list):
        return json.loads(self.client_list)

    class Meta:
        verbose_name = 'Home Footer'
        verbose_name_plural = 'Home Footer'


class OrderItem(BaseModel):
    sub_service = models.ForeignKey('SubService', help_text='Multiple Categories', related_name='orderitems_sub_service')
    price = models.ForeignKey('Pricing', help_text='Multiple Categories', related_name='orderitems_sub_service',
                              blank=True, null=True,on_delete=models.SET_NULL)
    orderprice = models.IntegerField(default=0)


class Orders(BaseModel):
    custom_user = models.ForeignKey(CustomBaseUser, blank=True, null=True, related_name="custom_user_orders")
    orderitems = models.ManyToManyField(OrderItem, help_text='Multiple Categories', related_name='order_sub_service')
    comments = models.TextField(null=True, blank=True, help_text='Comment for end user')
    event_date = models.DateField(auto_now_add=True)
    custom_message = models.TextField(null=True, blank=True, help_text='Enter Custom Message')
    shipping_address = models.TextField(null=True, blank=True, help_text='Enter Shipping Address')


    class Meta:
        verbose_name = 'Order'
        verbose_name_plural = 'User Orders'

    def __str__(self):
        if self.custom_user:
            return u'%s' % self.custom_user.first_name
        else:
            return u'%s' % self.comments


class ContactUs(BaseModel):
    email = models.EmailField(max_length=255)
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=16)
    company = models.CharField(max_length=100, null=True, blank=True)
    street1 = models.CharField(max_length=100, null=True, blank=True)
    street2 = models.CharField(max_length=200, null=True, blank=True)
    comment = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name = 'Contact Us'
        verbose_name_plural = 'Contact Us'


class Pricing(BaseModel):
    name = models.CharField(max_length=250)
    mrp = models.IntegerField(default=0)
    price_type = models.CharField(max_length=250, default='')
    discount = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    delivery_cost = models.IntegerField(default=0)
    service_tax = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    commission = models.DecimalField(decimal_places=2, max_digits=10, default=0)
    booking_amount = models.IntegerField(default=0)
    subservice = models.ManyToManyField(SubService, through='SubServicePrice')

    def __str__(self):
        return self.name

    @property
    def get_booking_amt(self):
        return self.booking_amount

    @property
    def get_discounted_price(self):
        if self.mrp > 0 and self.discount > 0:
            mrp=self.mrp-(self.discount/100)*self.mrp
            return int(mrp + (mrp * (self.service_tax / 100))) + self.delivery_cost
        else:
            return 0

    @property
    def final_amount(self):
        return int(self.mrp + (self.mrp * (self.service_tax / 100))) + self.delivery_cost


class SubServicePrice(models.Model):
    pricing = models.ForeignKey(Pricing, help_text='Multiple Categories', related_name='order_sub_service',null=True)
    subservice = models.ForeignKey(SubService, related_name='subservice_price')


class PricingSpec(BaseModel):
    """
    Represents pricing specification attribute
    """
    pricing = models.ForeignKey(Pricing, related_name='pricing_spec')
    name = models.ForeignKey(SpecName)
    value = models.CharField(max_length=250)
    display_order = models.IntegerField(default=0)
    is_filterable = models.BooleanField(default=False)

    class Meta:
        ordering = ('display_order', 'id',)
        unique_together = ('pricing', 'name',)
        verbose_name = 'Pricing Spec'
        verbose_name_plural = 'Pricing Specs'

    def __str__(self):
        return '%s: %s' % (self.name, self.value)