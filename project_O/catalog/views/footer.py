import json
import logging
from django.http import HttpResponse
from catalog.models import  HomeFooter
from user_info.models import VendorUser
from catalog.models import ContactUs
from django.shortcuts import render
from annoying.functions import get_object_or_None
from django.conf import settings
import os
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404
from catalog.views.home import get_occasion, get_outing, get_cart, get_offer

logging = logging.getLogger(__name__)


def faq(request, template="faq.html"):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()
    cart = get_cart(request)
    QA_list = []
    qa_obj = HomeFooter.objects.filter().exclude(faq_question=None, faq_answer=None)
    for qa in qa_obj:
        QA_list.append({'question': qa.faq_question, 'answer': qa.faq_answer})

    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'cart':cart, 'QA': QA_list}
    return render(request, template, data)


def contact_us(request, template="contact_us.html"):
    if request.method == 'POST':
        email = (request.POST.get('email', False))
        firstname = (request.POST.get('firstname', False))
        mobile = (request.POST.get('mobile', False))
        company = (request.POST.get('company', False))
        street1 = (request.POST.get('street1', False))
        street2 = (request.POST.get('street2', False))
        comment = (request.POST.get('comment', False))

        print("Email %s %s %s %s %s %s %s"% (email, firstname, mobile, company, street1, street2, comment))

        contacts = ContactUs(name=firstname, email=email, company=company, street1=street1, street2=street2,
                             comment=comment, mobile=mobile)
        contacts.save()
        return HttpResponse("We will get back to you Shortly. Thank You!")

    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()
    cart = get_cart(request)
    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'cart':cart}
    return render(request, template, data)


def about_us(request, template="about_us.html"):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()
    cart = get_cart(request)
    heading = "About Us"

    about_us = HomeFooter.objects.filter().exclude(about_us=None)
    content = []
    for about in about_us:
        content.append({'name': about.about_us})
    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'cart':cart, 'heading': heading,
            'content': content}
    return render(request, template, data)


def terms(request, template="about_us.html"):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()
    cart = get_cart(request)
    heading = "Terms & Conditions"

    terms = HomeFooter.objects.filter().exclude(about_us=None)
    content = []
    for term in terms:
        content.append({'name': term.terms_condtions})

    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'cart':cart, 'heading': heading,
            'content':content}
    return render(request, template, data)