import json
import logging
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from catalog.models import Category, CategoryBanners, Home, HomeFooter, Services, SubService, ServicePic, ServiceSpec, \
    Review
from user_info.models import VendorUser
from collections import OrderedDict
from project_O.settings import occassion_key, outing_key, offers_key, options_key
from django.shortcuts import render
from annoying.functions import get_object_or_None
from django.conf import settings
import os
from sales.models import Cart, CartItem
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import pdb

logging = logging.getLogger(__name__)


def handler404(request):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()
    cart = get_cart(request)

    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list,
            'cart': cart}

    response = render_to_response('404error.html', data,
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response


def handler500(request):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()
    cart = get_cart(request)

    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list,
            'cart': cart}

    response = render_to_response('404error.html', data,
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response

def get_occasion():
    occasion_list = []
    occassion_obj = Category.objects.filter(name=occassion_key)
    if occassion_obj:
        category_obj = Category.objects.filter(parent=occassion_obj, is_active=True)
        if category_obj:
            for co in category_obj:
                name = co.name
                slug = co.slug
                if (co.image):
                    image = co.image.url
                else:
                    image = ''
                occasion_list.append({'slug': slug, 'image': image, 'name': name})
    return occasion_list


def get_outing():
    outing_list = []
    outing_obj = Category.objects.filter(name=outing_key)
    if outing_obj:
        category_obj = Category.objects.filter(parent=outing_obj, is_active=True)
        if category_obj:
            for co in category_obj:
                name = co.name
                slug = co.slug
                if (co.image):
                    image = co.image.url
                else:
                    image = ''
                outing_list.append({'slug': slug, 'image': image, 'name': name})
    return outing_list


def get_offer():
    offer_list = []
    offer_obj = Category.objects.filter(name=offers_key)
    if offer_obj:
        category_obj = Category.objects.filter(parent=offer_obj, is_active=True)
        if category_obj:
            for co in category_obj:
                description = co.description
                name = co.name
                slug = co.slug
                if (co.image):
                    image = co.image.url
                else:
                    image = ''
                offer_list.append({'slug': slug, 'image': image, 'name': name, 'description': description})
    return offer_list


def get_services():
    services_list = []
    category_obj = Category.objects.filter(parent=None, is_active=True)
    for level1 in category_obj:
        sub_category_obj = Category.objects.filter(parent=level1, is_active=True)
        for level2 in sub_category_obj:
            if len(settings.HOME_SERVICES_OFFERED) > 0 and level2.name.lower() != settings.HOME_SERVICES_OFFERED.lower():
                continue
            leaf_category_obj = Category.objects.filter(parent=level2, is_active=True)
            for level3 in leaf_category_obj:
                name = level3.name
                slug = level3.slug
                if level3.logo:
                    logo = level3.logo.url
                else:
                    logo = ""
                url = os.path.join(level1.slug, level2.slug, level3.slug)
                services_list.append({'name': name, 'slug': slug, 'logo': logo, 'url': url})
    return services_list


def get_banner():
    banner_list = []
    home_banner_obj = Home.objects.filter(image_type="Banner", is_active=True)
    for banner in home_banner_obj:
        if banner.image:
            image = banner.image.url
        header_text = banner.header_text
        middle_text = banner.middle_text
        footer_text = banner.footer_text
        action_url = banner.action_url
        button1_text = banner.button1_text
        button2_text = banner.button2_text
        button1_action_url = banner.button1_action_url
        button2_action_url = banner.button2_action_url
        banner_list.append(
            {'image': image, 'header_text': header_text, 'middle_text': middle_text, 'footer_text': footer_text,
             'action_url': action_url, 'button1_text': button1_text, 'button1_action_url': button1_action_url,
             'button2_text': button2_text, 'button2_action_url': button2_action_url})

    return banner_list


def get_offer_promotions():
    extra_list = []
    promo_obj = get_object_or_None(Home, image_type="Promotions", is_active=True)
    if promo_obj:
        image = ""
        if promo_obj.image:
            image = promo_obj.image.url
        extra_list.append({'image': image, "action_url": promo_obj.action_url})

    offer_obj = get_object_or_None(Home, image_type="Offers", is_active=True)
    if offer_obj:
        image = ""
        if offer_obj.image:
            image = offer_obj.image.url
        extra_list.append({'image': image, "action_url": offer_obj.action_url})
    return extra_list


def get_cart(request):
    cart = None
    if 'cart_id' in request.session:
        cart_id = int(request.session['cart_id'])
        cart = Cart.get_cart(cart_id)
    else:
        cart = Cart.get_cart()
        request.session['cart_id'] = cart.id
    return cart


def home(request, template="index.html"):
    if request.method == 'GET':
        occasion_list = []
        outing_list = []
        offer_list = []
        services_list = []
        banner_list = []
        extra_list = []
        try:
            occasion_list = get_occasion()
            outing_list = get_outing()
            offer_list = get_offer()
            services_list = get_services()
            banner_list = get_banner()
            extra_list = get_offer_promotions()
            cart = get_cart(request)
        except:
            raise Http404;

        data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'services': services_list,
                'banners': banner_list, 'extras': extra_list, 'cart': cart}
        # print(data)
        return render(request, template, data)


def get_default_slug(slug2):
    slug3 = None
    parent_cat = get_object_or_None(Category, slug=slug2)
    if parent_cat is not None:
        category_obj = Category.objects.filter(parent=parent_cat)
        if category_obj:
            slug3 = category_obj[0].slug
    return slug3


def get_second_screen_banner(category_obj):
    banner_list = []
    banners = CategoryBanners.objects.filter(category=category_obj)
    image = None

    for banner in banners:
        if banner.banner:
            image = banner.banner.url
        text = banner.text
        banner_list.append({'image': image, 'text': text})
    return banner_list


def get_navigation_headings(slug, current_url, sub_service=None):
    navigation_headings = []
    category_obj = get_object_or_None(Category, slug=slug)
    url = None

    if category_obj:
        parent = category_obj.parent
        while parent is not None:
            total_len = current_url.find(parent.slug) + len(parent.slug) + 1
            prev_url = url
            url = current_url[:total_len]

            if parent.parent is None:
                url = prev_url

            navigation_headings.append({'name': parent.name, 'url': url})
            parent = parent.parent

        navigation_headings.reverse()

        if current_url.find(category_obj.slug) == -1:
            url = navigation_headings[-1]['url']
        else:
            total_len = current_url.find(category_obj.slug) + len(category_obj.slug) + 1
            url = current_url[:total_len]
        navigation_headings.append({'name': category_obj.name, 'url': url})

    if sub_service:
        sub_service_obj = get_object_or_None(SubService, slug=sub_service)
        total_len = current_url.find(sub_service_obj.slug) + len(sub_service_obj.slug) + 1
        url = current_url[:total_len]
        navigation_headings.append({'name': sub_service_obj.name, 'url': url})

    return navigation_headings

def displayprice(pricing):
    if pricing.mrp > 0 and pricing.discount > 0:
        show_pretext="&#8377;"
        show_price=pricing.get_discounted_price
        show_posttext=pricing.price_type
    elif pricing.mrp > 0:
        show_pretext="&#8377;"
        show_price=pricing.final_amount
        show_posttext=pricing.price_type
    elif pricing.discount > 0:
        show_pretext=""
        show_price=int(pricing.discount)
        show_posttext=pricing.price_type
    else:
        show_pretext=""
        show_price="Contact for price"
        show_posttext=""
    return [show_pretext,show_price,show_posttext]


def second_screen(request, slug1, slug2, slug3=None, template="second_screen.html"):
    if slug3 is None:
        slug3 = get_default_slug(slug2)
    try:
        page = request.GET.get('page')
        if page is None:
            page = 1
        sort = request.GET.get('sort_by')
        if sort is None or sort == 'None' or sort == 'undefined':
            sort = None
        filter_by = request.GET.get('filter_by')
        occasion_list = get_occasion()
        outing_list = get_outing()
        offer_list = get_offer()

        category_obj = get_object_or_None(Category, slug=slug3, is_active=True)

        tags = category_obj.tags
        general_description = category_obj.description

        navigation_headings = get_navigation_headings(slug3, request.get_full_path(), None)

        sub_service_list = []

        banner_list = get_second_screen_banner(category_obj)

        left_menu_list = []
        left_menu = Category.objects.filter(parent=category_obj.parent, is_active=True)
        for sub_cat in left_menu:
            url = os.path.join("/", slug1, slug2, sub_cat.slug)
            left_menu_list.append({'name': sub_cat.name, 'url': url})

        heading = None
        total_count=0
        if left_menu:
            heading = category_obj.name
            service_obj = Services.objects.filter(category=category_obj, is_active=True).values_list('id', flat=True)
            kwargs={}
            kwargs['service__in']=service_obj
            kwargs['is_active']=True
            total_count=SubService.objects.filter(**kwargs).count()
            if filter_by is not None:
                kwargs['service_spec__value__icontains']=filter_by

            if (sort):
                sub_service_obj = SubService.objects.filter(**kwargs).order_by(sort)
            else:
                sub_service_obj = SubService.objects.filter(**kwargs)
                #sub_service_obj = SubService.objects.filter(service__in=service_obj, is_active=True,service_spec__value__icontains=filter_by)


            paginator = Paginator(sub_service_obj, settings.CONTENTS_PER_PAGES)

            try:

                sub_service_obj = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                sub_service_obj = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                sub_service_obj = paginator.page(paginator.num_pages)


            for sub_service in sub_service_obj:
                jsondata = {}
                pricingset=sub_service.pricing_set.order_by('mrp')
                service_pic_obj = ServicePic.objects.filter(sub_service=sub_service)
                jsondata['id'] = sub_service.pk
                jsondata['name'] = sub_service.name
                jsondata['slug'] = sub_service.slug
                jsondata['avg_rating'] = sub_service.avg_rating

                jsondata['price']=[]
                for pricing in pricingset:
                    p={}
                    p['type']=pricing.price_type
                    p['mrp']=pricing.final_amount
                    p['discounted_price']=pricing.get_discounted_price
                    p['displayprice']=displayprice(pricing)
                    p['discount']=int(pricing.discount)
                    jsondata['price'].append(p)

                mrp = float(sub_service.mrp)
                discounted_price = float(sub_service.discounted_price)

                image = None
                if len(service_pic_obj) > 0 and service_pic_obj[0].image:
                    image = service_pic_obj[0].image.url
                else:
                    image=settings.DEFAULT_IMAGE
                jsondata['image']=image
                url = os.path.join("/",slug1,slug2,slug3,sub_service.slug)
                jsondata['url']=url
                sub_service_list.append(jsondata)
                '''    {'id':id, 'name': name, 'slug': slug, 'image': image, 'mrp': mrp, 'discounted_price': discounted_price,
                     'rating_count': rating_count,'url':url})
                '''

            cart = get_cart(request)
            if sub_service_obj.has_next():
                page = int(page) + 1
            else:
                page = 0

            #refine search
            totalfilters=ServiceSpec.objects.filter(is_filterable=True,service__in=SubService.objects.filter(service__in=Services.objects.filter(category=category_obj)))
            search=OrderedDict()
            if len(totalfilters) > 0 :
                search["all"]=["/"+slug1+"/"+slug2+"/"+slug3+"/",total_count]

                for filter in totalfilters:
                    search[filter.value]=["/"+slug1+"/"+slug2+"/"+slug3+"/?filter_by="+filter.value,ServiceSpec.objects.filter(value__icontains=filter.value).count()]


            data = {'tags': tags, 'general_description': general_description, 'occasions': occasion_list,
                    'outings': outing_list,
                    'offers': offer_list,
                    'banners': banner_list, 'left_menu': left_menu_list, 'heading': heading, 'leaf': sub_service_list,
                    'navigation_headings': navigation_headings, 'cart': cart, 'page': page, 'sort': sort,'search':search}
            if request.is_ajax():
                return render(request, "grid_display.html", data)
    except Exception as e:
        raise Http404

    # print(sub_service_list)

    return render(request, template, data)


def get_service_pic(sub_service_obj):
    image_list = []
    service_pic_obj = ServicePic.objects.filter(sub_service=sub_service_obj)
    first_image = None
    if service_pic_obj[0].image:
        first_image = service_pic_obj[0].image.url

    for service_pic in service_pic_obj:
        image = None
        if service_pic.image:
            image = service_pic.image.url
        image_list.append({'image': image})
    return first_image, image_list


def get_specs(sub_service_obj):
    spec_list = []
    for specs in sub_service_obj.service_spec.all():
        if specs.name is None or specs.value is None:
            continue
        name = specs.name
        value = specs.value
        spec_list.append({'name': name, 'value': value})
    return spec_list



def get_related_products(sub_service_obj,slug1,slug2,slug3):
    related_products_list = []
    categories = sub_service_obj.service.category.all()

    sub_services = SubService.objects.filter(service=Services.objects.filter(category__in=categories), is_active=True).exclude(
        pk=sub_service_obj.pk)[:10]

    for sub_service in sub_services:
        jsondata={}
        # print(sub_service.is_active)
        image = None
        service_pic_obj = ServicePic.objects.filter(sub_service=sub_service)
        url=os.path.join("/",slug1,slug2,slug3,sub_service.slug)
        if len(service_pic_obj) > 0 and service_pic_obj[0].image:
            image = service_pic_obj[0].image.url
        pricingset=sub_service.pricing_set.all()

        jsondata['price']=[]
        jsondata['name'] = sub_service.name
        jsondata['avg_rating'] = sub_service.avg_rating
        jsondata['image'] = image
        jsondata['url'] = url

        for pricing in pricingset:
            p={}
            p['type']=pricing.price_type
            p['mrp']=pricing.final_amount
            p['discounted_price']=pricing.get_discounted_price
            jsondata['price'].append(p)

        related_products_list.append(jsondata)

    return related_products_list

def get_prev_next_obj(cur_obj, slug1, slug2, slug3):
    sub_service_prev_next_list = []
    prev_url = next_url = None

    category_obj = get_object_or_None(Category, slug=slug3, is_active=True)
    service_objs = Services.objects.filter(category=category_obj, is_active=True).values_list('id', flat=True)
    sub_service_obj = SubService.objects.filter(service__in=service_objs, is_active=True)

    for sub_service in sub_service_obj:
        id = sub_service.pk
        url = os.path.join("/", slug1, slug2, slug3, sub_service.slug)
        if cur_obj.id == id:
            sub_service_prev_next_list.append(id)
        else:
            sub_service_prev_next_list.append({'id': id, 'url': url})

    if cur_obj.id in sub_service_prev_next_list:
        index = sub_service_prev_next_list.index(cur_obj.id)
        if index - 1 >= 0:
            previous_value = sub_service_prev_next_list[index - 1]
            prev_url = previous_value['url']
        else:
            prev_url = None
        if index + 1 < len(sub_service_prev_next_list):
            next_value = sub_service_prev_next_list[index + 1]
            next_url = next_value['url']
        else:
            next_url = None

        # print(prev_url)
        # print(next_url)
    return prev_url, next_url

def nested_set(dic, keys, value):
    for key in keys[:-1]:
        dic = dic.setdefault(key, {})
    dic[keys[-1]] = value


def third_screen(request, slug1, slug2, slug3, ssname, template="third_screen.html"):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()

    data = {}

    sub_service_obj = get_object_or_None(SubService, slug=ssname)

    prev_url, next_url = get_prev_next_obj(sub_service_obj, slug1, slug2, slug3)

    navigation_headings = get_navigation_headings(slug3, request.get_full_path(), ssname)

    category_obj = get_object_or_None(Category, slug=slug3, is_active=True)

    general_description = category_obj.description

    if sub_service_obj:

        tags = sub_service_obj.tags

        jsondata = {}
        pricingset=sub_service_obj.pricing_set.all().order_by('mrp')
        jsondata['price']=[]
        tspecs=OrderedDict()
        finalpricing=OrderedDict()
        for pricing in pricingset:
            p={}
            p['id']=pricing.id
            p['type']=pricing.price_type
            p['mrp']=pricing.final_amount
            p['discount']=int(pricing.discount)
            p['discounted_price']=pricing.get_discounted_price
            p['specs']=[]
            p['displayprice']=displayprice(pricing)

            pspec=pricing.pricing_spec.all().order_by('id')
            speclist=[]

            for spec in pspec:
                if str(spec.name) not in tspecs:
                   tspecs[str(spec.name)]=[]
                if str(spec.value) not in tspecs[str(spec.name)]:
                    tspecs[str(spec.name)].append(spec.value)

                speclist.append(spec.value)
                p['specs'].append([str(spec.name), spec.value])
            s={}


            if len(speclist) > 0:
                nested_set(s, speclist, [p['mrp'],pricing.id,pricing.price_type,int(pricing.discount),pricing.get_discounted_price])

                for key, value in s.items():
                    if key in finalpricing:
                        finalpricing[key].update(value)
                    else:
                        finalpricing=OrderedDict(finalpricing, **s)

            jsondata['price'].append(p)

        service_name = sub_service_obj.service.vendor_user.name
        id = sub_service_obj.pk
        name = sub_service_obj.name
        gist = sub_service_obj.gist
        discounted_price = float(sub_service_obj.discounted_price)
        mrp = float(sub_service_obj.mrp)
        description = sub_service_obj.description
        if description is None:
            description=gist

        vt_url = sub_service_obj.virtual_tour_url

        total_user_reviews = Review.objects.filter(reviewed_item=sub_service_obj, is_active=True, is_verified=True)

        user_reviews = total_user_reviews.order_by('-created_on')[:10]
        reviews_count = len(total_user_reviews)
        average_rating = sub_service_obj.avg_rating

        try:
            first_image, image_list = get_service_pic(sub_service_obj)
        except:
            first_image = settings.DEFAULT_IMAGE
            image_list = []

        spec_list = get_specs(sub_service_obj)
        try:
            related_products_list = get_related_products(sub_service_obj, slug1, slug2, slug3)
        except:
            related_products_list = []

        cart = get_cart(request)
        data = {'tags': tags, 'general_description': general_description, 'occasions': occasion_list,
                'outings': outing_list,
                'offers': offer_list, 'name': name, 'gist': gist,
                'price':jsondata['price'], 'description': description, 'first_image': first_image,
                'images': image_list, 'product_specs': spec_list,
                'related_products': related_products_list,'id': id, 'cart':cart,
                'vt_url':vt_url,'ssname':ssname,
                'navigation_headings': navigation_headings,
                'service_name':service_name, 'prev_url': prev_url, 'next_url':next_url,
                'specs':tspecs, 'user_reviews': user_reviews, 'reviews_count': reviews_count,
                'average_rating': average_rating,
                'finalpricing':json.dumps(finalpricing)

                }
    # print (data)
    return render(request, template, data)


@csrf_exempt
def user_review(request):
    if request.method == 'POST':
        try:
            data = request.body
            recieved_data = json.loads(data.decode("ascii", "ignore"))
            sub_service_id = recieved_data.get('sub_service_id', None)
            price_rating = recieved_data.get('price_rating', None)
            quality_rating = recieved_data.get('quality_rating', None)
            nickname = recieved_data.get('nickname', None)
            summary = recieved_data.get('summary', None)
            comment = recieved_data.get('comment', None)

            if sub_service_id:
                # print("SS id %s " % sub_service_id)
                sub_service_obj = get_object_or_None(SubService, pk=sub_service_id)
                if sub_service_obj:
                    user_review_obj = Review(reviewed_item=sub_service_obj, price_rating=price_rating,
                                             quality_rating=quality_rating, nickname=nickname, summary=summary,
                                             comment=comment, is_active=True)
                    user_review_obj.save()

            return HttpResponse("Success", status=200)

        except Exception as e:
            return HttpResponse("Failure", status=200)
    else:
        return HttpResponse("Get Method not supported")


def virtual_tour(request, ssname, template="virtual_tour.html"):
    try:
        occasion_list = get_occasion()
        outing_list = get_outing()
        offer_list = get_offer()
        sub_service_obj = get_object_or_None(SubService, slug=ssname)
        data = {}
        url = sub_service_obj.virtual_tour_url
        cart = get_cart(request)

        data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'url': url, 'cart': cart}
        return render(request, template, data)
    except:
        raise Http404
