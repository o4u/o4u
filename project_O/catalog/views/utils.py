import csv
import sys
import pdb
# name,mrp,discounted_price,gist,description,latitude,longitude,quantity,display_order,Price,Tax,Images
from catalog.models import Category, CategoryBanners, Home, HomeFooter, Services, SubService, ServicePic, ServiceSpec, SubServicePrice, Pricing, PricingSpec, SpecName
from django.template.defaultfilters import slugify
from django.core.files import File
from django.db import transaction
from os.path import expanduser


@transaction.atomic
def dump(filename,sname=None):
    home = expanduser("~")
    s=Services.objects.get(name=sname)
    with open(home+filename, 'rt') as f:
        reader = csv.DictReader(f)
        for idx,row in enumerate(reader):
            print("---------")
            print("name:", row['name'])
            print("mrp:", row['mrp'].replace("/-",""))
            print("gist:", row['gist'])
            print("description:", "<br>".join(str(row['description']).split("\n")))
            print("Image:", row['Images'])

            name=row['name']
            mrp=row['mrp']
            gist="<br>".join(str(row['gist']).split("\n"))
            desc="<br>".join(str(row['description']).split("\n"))
            img=row['Images'].split(",")
            ss=SubService(name=name,slug=slugify(name)+"-"+str(idx),mrp=mrp,gist=gist,description=desc,service=s)
            ss.save()
            for i in img:
                sp=ServicePic(sub_service=ss)
                with open(home+"/new/"+i,"rb") as r:
                    django_file = File(r)
                    sp.image.save(i,django_file, save=True)
                    sp.save()

@transaction.atomic
def dumpcake(filename,filename2,sname=None):
    home = expanduser("~")
    service=Services.objects.get(name=sname)
    gist="Inclusive of all taxes and delivery charges."
    with open(home+filename, 'rt') as f:

        reader1 = csv.DictReader(f)
        for idx1,row1 in enumerate(reader1):
            #pdb.set_trace()
            name=row1['name']
            img=row1['Images'].split(",")

            ss=SubService(name=name,slug=slugify(name)+"-"+str(idx1),gist=gist,service=service)
            ss.save()
            for i in img:
                sp=ServicePic(sub_service=ss)
                with open(home+"/cakes/"+i,"rb") as r:
                    django_file = File(r)
                    sp.image.save(i,django_file, save=True)
                    sp.save()
            with open(home+filename2, 'rt') as f1:
                    reader = csv.DictReader(f1)
                    for idx,row in enumerate(reader):

                        for ii in range(2,6):
                            i=str(ii)
                            mrp=row[i+'Kg/Egg']
                            if int(mrp) > 0:
                                p=Pricing(name=ss.name+ i,mrp=int(mrp)+350,price_type="/-",service_tax=20,delivery_cost=150)
                                p.save()

                                ps=PricingSpec(pricing=p,name=SpecName.objects.get(id=24),value=i+'Kg/Egg')
                                ps.save()

                                ps=PricingSpec(pricing=p,name=SpecName.objects.get(id=25),value=row['Flavours'])
                                ps.save()

                                s=SubServicePrice(pricing=p,subservice=ss)
                                s.save()

                            mrp=row[i+'Kg/Eggless']
                            if int(mrp) > 0:
                                p=Pricing(name=ss.name+ i,mrp=int(mrp)+350,price_type="",service_tax=20,delivery_cost=150)
                                p.save()
                                ps=PricingSpec(pricing=p,name=SpecName.objects.get(id=25),value=row['Flavours'])
                                ps.save()

                                ps=PricingSpec(pricing=p,name=SpecName.objects.get(id=24),value=i+'Kg/Eggless')
                                ps.save()




                                s=SubServicePrice(pricing=p,subservice=ss)
                                s.save()