import json
import logging
from annoying.functions import get_object_or_None
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from catalog.models import Category, CategoryBanners

logging = logging.getLogger(__name__)



@csrf_exempt
def get_second_level(request):

    if request.method == 'POST':

        # data = {'status': 400}

        post_data = request.body
        post_data_obj = json.loads(post_data.decode("ascii", "ignore"))
        logging.info("Post Data received: %s" % post_data_obj)

        slug_name = post_data_obj.get('slug_name', None)

        final_data = {}
        banner_list = []
        level3_list = []

        if slug_name:
            logging.info("get_second_level: slug_name %s" % slug_name)
            category_obj = get_object_or_None(Category, slug=slug_name, is_active=True)

            level3 = Category.objects.filter(parent=category_obj)
            logging.info(level3)
            for child in level3:
                level3_list.append({"name": child.name, "slug": child.slug})

            if category_obj:
                category_banners_obj = CategoryBanners.objects.filter(category=category_obj, is_active=True)
                for cbs in category_banners_obj:
                    banner_list.append({"image": cbs.banner.url, "text": cbs.text, "display_order": cbs.display_order})

                final_data['level3'] = level3_list
                final_data['banners'] = banner_list

        else:
            logging.error("get_second_level: slug_name is None")

        return HttpResponse(json.dumps(final_data), content_type='application/json')


@csrf_exempt
def get_third_level(request):

    if request.method == 'POST':

        post_data = request.body
        post_data_obj = json.loads(post_data.decode("ascii", "ignore"))
        logging.info("Post Data received: %s" % post_data_obj)

        slug_name = post_data_obj.get('slug_name', None)
        if slug_name:
            logging.info("get_second_level: slug_name %s" % slug_name)
            category_obj = get_object_or_None(Category, slug=slug_name, is_active=True)

        return HttpResponse("test")
