from django.conf.urls import include, url
# from catalog.api import LocationResource, VendorResource, CategoryResource, ServicesResource, SpecNameResource, ServiceSpecResource, ServicePicResource
# from tastypie.api import Api


# v1_api = Api(api_name='v1')
#
# v1_api.register(CategoryResource())
# v1_api.register(LocationResource())
# v1_api.register(VendorResource())
# v1_api.register(ServicesResource())
# v1_api.register(SpecNameResource())
# v1_api.register(ServiceSpecResource())
# v1_api.register(ServicePicResource())

from catalog.views.home import user_review

urlpatterns = [
    # url(r'^api/', include(v1_api.urls)),
    url(r'^api/get_second_level$', 'catalog.views.get_level.get_second_level', name="get-second-level"),
    url(r'^api/get_third_level$', 'catalog.views.get_level.get_third_level', name="get-third-level"),
    url(r'^get_occassion_category', 'catalog.views.home.get_occassion_category', name="get-occasion-category"),
    url(r'^get_outing_category', 'catalog.views.home.get_outing_category', name="get-outing-category"),
    url(r'^get_offers_category', 'catalog.views.home.get_offers_category', name="get-offers-category"),
    url(r'^get_options_category', 'catalog.views.home.get_options_category', name="get-options-category"),
    url(r'^get_menu_submenu$', 'catalog.views.home.get_menu_submenu', name="get-menu-submenu"),
    url(r'^get_dynamic_content', 'catalog.views.home.get_dynamic_content', name="get-dynamic-content"),
    url(r'^home_third_screen', 'catalog.views.home.third_screen', name="home-third-screen"),
    url(r'^home_second_screen', 'catalog.views.home.second_screen', name="home-second-screen"),
    url(r'^user_review$', user_review, name="user-review"),
]
