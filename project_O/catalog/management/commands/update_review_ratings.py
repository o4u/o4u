from django.core.management.base import BaseCommand, CommandError
from catalog.models import Review, SubService
from django.http import Http404


class Command(BaseCommand):
    help = "Calculates the Reviews for all the sub services and updates the average of the ratings"

    def handle(self, *args, **options):
        try:
            sub_service_objs = SubService.objects.filter(is_active=True)
            for sub_service_obj in sub_service_objs:
                total_user_reviews = Review.objects.filter(reviewed_item=sub_service_obj, is_active=True,
                                                           is_verified=True)
                average_rating = 0
                if total_user_reviews:
                    average_rating = Review.calculate_average_rating(total_user_reviews)
                    sub_service_obj.avg_rating = average_rating
                else:
                    sub_service_obj.avg_rating = 0
                sub_service_obj.save()

            print("successfully executed handler!!")
        except Exception as e:
            print("Exception occured! ")
