from django.contrib import admin

from catalog.models import Category, CategoryBanners, Services, SubService, SpecName, ServiceSpec, ServicePic, Home,\
    HomeFooter, Orders, ContactUs, PricingSpec, Pricing, Review,OrderItem


class CategoryBannersAdmin(admin.TabularInline):
    model = CategoryBanners


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'image_info', 'logo_info', 'parent', 'display_order', 'is_active')
    list_filter = ['name']
    search_fields = list_filter
    prepopulated_fields = {'slug': ('name', 'parent')}
    inlines = [
        CategoryBannersAdmin,
    ]


class ServicePicAdmin(admin.TabularInline):
    model = ServicePic


class ServiceSpecAdmin(admin.TabularInline):
    model = ServiceSpec


class ServicesAdmin(admin.ModelAdmin):
    # exclude = ['slug']
    filter_horizontal = ['category']
    list_display = ('name', 'slug', 'is_active')
    list_filter = list_display
    search_fields = list_display
    prepopulated_fields = {'slug': ('name', 'vendor_user')}

#
# class SubServiceAdmin(admin.ModelAdmin):
#     # exclude = ['slug']
#     list_display = ('name', 'slug', 'is_active', 'is_bestseller', 'is_featured')
#     list_filter = ('name', 'is_active', 'is_bestseller', 'is_featured')
#     search_fields = ('name', 'is_active', 'is_bestseller', 'is_featured')
#     prepopulated_fields = {'slug': ('name', 'service')}
#     inlines = [
#         ServicePicAdmin,
#         ServiceSpecAdmin,
#     ]


class SpecNameAdmin(admin.ModelAdmin):
    list_display = ('name', 'data_type')
    list_filter = list_display
    search_fields = ['name']

class HomeAdmin(admin.ModelAdmin):
    list_display = ('name', 'image','image_type', 'is_active', 'display_order')
    list_filter = list_display
    search_fields = ['version']


class HomeFooterAdmin(admin.ModelAdmin):
    list_display = ('client_list', 'contact_us', 'faq_question','faq_answer')
    list_filter = list_display


class ContactUsAdmin(admin.ModelAdmin):
    list_display = ('email', 'mobile', 'name')


class OrdersAdmin(admin.ModelAdmin):
    list_display = ['getorderitems', 'event_date', 'comments','event_date']

    def getorderitems(self, obj):
        return " || ".join([s.sub_service.name for s in obj.orderitems.all()])


class PricingSpecAdmin(admin.TabularInline):
    model = PricingSpec
    extra = 1
    fk_name = 'pricing'


class PricingAdmin(admin.ModelAdmin):
    model = Pricing
    extra = 1

    inlines = [
        PricingSpecAdmin,

    ]


class SubServicePriceAdmin(admin.TabularInline):
    model = Pricing.subservice.through


class SubServiceAdmin(admin.ModelAdmin):
    # exclude = ['slug']
    list_display = ('name', 'slug', 'is_active', 'is_bestseller', 'is_featured')
    list_filter = ('name', 'is_active', 'is_bestseller', 'is_featured')
    search_fields = ('name', 'is_active', 'is_bestseller', 'is_featured')
    prepopulated_fields = {'slug': ('name', 'service')}
    inlines = [
        ServicePicAdmin,
        ServiceSpecAdmin,
        SubServicePriceAdmin
    ]


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['reviewed_item', 'user', 'price_rating', 'quality_rating', 'is_verified', 'is_active', 'nickname',
                    'summary', 'comment']
    search_fields = ['is_verified', 'is_active', 'nickname']
    list_filter = ['price_rating', 'quality_rating']

class OrdersItemAdmin(admin.ModelAdmin):
    pass

admin.site.register(Pricing, PricingAdmin)
admin.site.register(Home, HomeAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Services, ServicesAdmin)
admin.site.register(SubService, SubServiceAdmin)
admin.site.register(SpecName, SpecNameAdmin)
admin.site.register(HomeFooter, HomeFooterAdmin)
admin.site.register(Orders, OrdersAdmin)
admin.site.register(OrderItem, OrdersItemAdmin)
admin.site.register(ContactUs, ContactUsAdmin)
admin.site.register(Review, ReviewAdmin)