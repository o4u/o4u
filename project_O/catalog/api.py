from tastypie.resources import ModelResource
from catalog.models import Category, Services, SpecName, ServiceSpec, ServicePic
from tastypie import fields
from user_info.models import Location, Vendor, BaseModel
from annoying.functions import get_object_or_None
import logging
logging = logging.getLogger(__name__)


class CategoryResource(ModelResource):

    logging.info("CategoryResource")

    class Meta:
        queryset = Category.objects.filter(is_active=True).exclude(parent=None)
        resource_name = 'category'
        fields = ['name', 'slug', 'description', 'image', 'logo', 'tags', 'display_order']
        allowed_methods = ['get']

    def dehydrate(self, bundle):
        if bundle.obj.parent:
            parent_data = str(bundle.obj.parent)
            parent_data = parent_data.split(' > ')
            if len(parent_data) > 1:
                bundle.data['grandparent'] = parent_data[0]
                bundle.data['parent'] = parent_data[1]
            else:
                bundle.data['grandparent'] = 'o4u'
                bundle.data['parent'] = bundle.obj.parent
        else:
            bundle.data['grandparent'] = 'o4u'
            bundle.data['parent'] = 'o4u'
        return bundle


class LocationResource(ModelResource):

    class Meta:
        queryset = Location.objects.all()
        resource_name = 'location'


class VendorResource(ModelResource):

    location = fields.ForeignKey(LocationResource, 'location')

    class Meta:
        queryset = Vendor.objects.all()
        resource_name = 'vendor'


class ServicesResource(ModelResource):

    category = fields.ToManyField(CategoryResource, 'category')

    class Meta:
        queryset = Services.objects.all()
        resource_name = 'services'

    def dehydrate(self, bundle):

        category_obj = get_object_or_None(Category, slug='flower-decorator-bday')
        bundle.data['category_name'] = category_obj.slug
        return bundle


class SpecNameResource(ModelResource):
    class Meta:
        queryset = SpecName.objects.all()
        resource_name = 'specname'


class ServiceSpecResource(ModelResource):
    class Meta:
        queryset = ServiceSpec.objects.all()
        resource_name = 'servicesspec'


class ServicePicResource(ModelResource):

    class Meta:
        queryset = ServicePic.objects.all()
        resource_name = 'servicepic'

