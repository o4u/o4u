function cart_update(id){
    var product_id=id;
    $.ajax({
        url: '/sales/cart/add/',
        type: 'POST',
        data: {'product_id':product_id},
        contentType: false,
        cache: false,
        processData: false,
        success: function (data, textStatus, jqXHR) {

        }
    })

}

function cart_update_delete(id){
    var product_id=id;
    $.ajax({
        url: '/sales/cart/delete/',
        type: 'POST',
        data: {'product_id':product_id},
        contentType: false,
        cache: false,
        processData: false,
        success: function (data, textStatus, jqXHR) {

        }
    })

}