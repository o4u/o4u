ls -d */ | while read line
do
convert "$line/pano.jpg" -resize 2000x "$line/pano1.jpg"
mv "$line/pano1.jpg" "$line/pano.jpg"

  convert watermark.png  -fill grey100 -colorize 20  miff:- |\
  composite -dissolve 15  -tile -gravity Center -  "$line/pano.jpg" "$line/pano_w.jpg"
mv "$line/pano_w.jpg" "$line/pano.jpg"
done
