find . -name *.jpg | while read line
do

  convert watermark.png  -fill grey100 -colorize 20  miff:- |\
  composite -dissolve 15  -gravity Center -  "$line" "$line"
done
