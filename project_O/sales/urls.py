from django.conf.urls import include, url

from sales.views import add_to_cart, remove_from_cart, display_cart, get_quote

urlpatterns = [
    url(r'^add', add_to_cart, name='sales_add_to_cart'),
    url(r'^remove', remove_from_cart, name='remove_from_cart'),
    url(r'^cart', display_cart, name='display_cart'),
    url(r'^get_quote$', get_quote, name='get_quote'),
]
