from django.shortcuts import render
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from sales.models import Cart, CartItem
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.http import HttpResponse
from catalog.views.home import get_cart, get_occasion, get_outing, get_offer
from user_info.models import CustomBaseUser
from catalog.models import Orders, SubService,OrderItem,Pricing
from annoying.functions import get_object_or_None
import json
from django.http import Http404
import datetime
import traceback
from django.core.mail import send_mail
from threading import Thread
from django.template import loader, Context
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
import uuid


def postpone(function):
    def decorator(*args, **kwargs):
        t = Thread(target = function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator

@postpone
def send_email(orders, custom_user):
    subject = "Order Confirmation - Your Order with O4U has been successfully placed!"
    body = "Thank you for your order! Your order is being processed. Our executive will get in touch with you shortly"
    product_details = []
    for item in orders.orderitems.all():
        product_id = str(uuid.uuid4().hex)[:7]
        price = item.orderprice
        if price == 0 :
            price = "Applicable Later"
        product_details.append({'product_id': product_id, 'product': item.sub_service.name, 'price': price, 'quantity':1})
    c = Context({'email': custom_user.email, 'body': body, 'product_details': product_details})
    email = loader.render_to_string('email.html', c)
    text_content = strip_tags(email)
    msg = EmailMultiAlternatives(subject, text_content,'', ['vidhiijain@gmail.com', 'bharat.chikku@gmail.com',
                                                            'j.deepesh@gmail.com'])
    msg.attach_alternative(email, "text/html")
    msg.send()


@csrf_exempt
def get_quote(request):
    if request.method == 'POST':
        data = request.body
        data_info = json.loads(data.decode("ascii", "ignore"))
        response_data = {}
        response_data['result'] = 'error'

        email = (data_info['email'])
        phone = (data_info['phone'])
        user_name = (data_info['user_name'])
        sub_service_id = (data_info['cart_items'])
        event_date = (data_info['event_date'])
        shipping_address = (data_info['shipping_address'])

        if 'cart_id' in request.session:
            cart_id = int(request.session['cart_id'])
            cart = Cart.get_cart(cart_id)
        else:
            response_data['message'] = "You cannot place order. There are no Items in Your Cart!"
            return HttpResponse(json.dumps(response_data),status=200)
        cart = get_cart(request)
        items = cart.get_items()

        if len(sub_service_id) == 0:
            response_data['message'] = "You cannot place order. There are no Items in Your Cart!"
            return HttpResponse(json.dumps(response_data),status=200)

        try:
            oilist=[]
            for item in items:
                if item.pricing.discount > 0:
                    price=item.pricing.get_discounted_price
                else:
                    price=item.pricing.final_amount
                oi=OrderItem(sub_service=item.product,price=item.pricing,orderprice=price)
                oi.save()
                oilist.append(oi)

            '''for id in sub_service_id:
                sub_service_obj = get_object_or_None(SubService, pk=id)
                sub_service_list.append(sub_service_obj)
            '''

            cu = get_object_or_None(CustomBaseUser,email=email)
            if cu is not None:
                cu.phone_no = phone
                cu.first_name = user_name
            else:
                cu = CustomBaseUser(email=email, first_name=user_name, phone_no=phone)
                cu.set_password("o4u123")
                cu.save()

            orders = Orders(custom_user=cu, shipping_address=shipping_address)
            orders.save()
            orders.event_date = datetime.datetime.strptime(event_date, '%d-%m-%Y')
            orders.orderitems.add(*oilist)
            orders.save()
            send_email(orders,cu)
        except Exception:
            traceback.print_exc()
            # print("Exception occured %s "% (Exception.err))
            response_data['message'] = "Invalid Details"
            return HttpResponse(json.dumps(response_data),status=200)
        request.session.flush()

    response_data['result'] = "Success"
    response_data['message'] = "Executive will Call you in few minutes"
    return HttpResponse(json.dumps(response_data),status=200)


def display_cart(request, template="display_cart.html"):
    occasion_list = get_occasion()
    outing_list = get_outing()
    offer_list = get_offer()

    cart = get_cart(request)
    items = cart.get_items()
    display_cart_list = []

    data = {}

    sub_total = 0.0
    for item in items:
        pk = item.pk
        name = item.product.name
        mrp = item.pricing.final_amount
        discounted_price = item.pricing.get_discounted_price
        discount = item.pricing.discount
        image = None
        if discounted_price is 0.0:
            sub_total += mrp
        else:
            sub_total += discounted_price

        try:
            image = item.product.service_pic.all()[0].image.url
        except Exception as e:
            print(str(e))

        display_cart_list.append({'id': pk, 'name':name,'discount':discount, 'mrp':mrp, 'discounted_price': discounted_price, 'image': image})
    sub_total=cart.get_sub_total()
    data = {'occasions': occasion_list, 'outings': outing_list, 'offers': offer_list, 'cart': cart,
            'display_cart_list': display_cart_list, 'sub_total': sub_total}
    # print(data)
    return render(request, template, data)

@csrf_exempt
def add_to_cart(request):
    """
    Add product to cart
    """

    product_id = int(request.POST['product_id'])
    price_id = int(request.POST['price_id'])

    # Checking if user already has cart in session
    # otherwise create a new cart for the user
    if 'cart_id' in request.session:
        cart_id = int(request.session['cart_id'])
        cart = Cart.get_cart(cart_id)
    else:
        cart = Cart.get_cart()
        request.session['cart_id'] = cart.id

    try:
        quantity = 1
        if quantity > 0:
            cart.add_item(product_id, quantity,price_id, request.user)
        else:
            raise ValueError()
    except ValueError:
        return HttpResponseBadRequest('Product quantity is not correct, please enter one or more products in numbers.')

    if request.is_ajax():
        return render(request, 'cart.html', {'cart': cart})

    return render(request, 'cart.html', {'cart': cart})


@transaction.atomic
def remove_from_cart(request):
    """
    Remove product from cart
    """
    product_id = int(request.POST['product_id'])
    # Checking if user session has cart or session may already flushed
    # Cart an empty cart for user
    if 'cart_id' in request.session:
        cart_id = int(request.session['cart_id'])
        cart = Cart.get_cart(cart_id)
        cart.remove_item(product_id)
    else:
        cart = Cart()

    if request.is_ajax():
        return render(request, 'cart.html', {'cart': cart})

    return render(request, 'cart.html', {'cart': cart})


@transaction.atomic
def remove_all_from_cart(request):
    """
    Remove all products from cart
    """
    if request.method == 'POST':
        if 'cart_id' in request.session:
            cart_id = int(request.session['cart_id'])
            cart = Cart.get_cart(cart_id)
            cart.remove_all_items()
        else:
            cart = Cart()

        if request.is_ajax():
            return render(request, 'sales/cart_basket.html', {'cart': cart})

    return HttpResponseRedirect(reverse('sales_checkout_cart'))

