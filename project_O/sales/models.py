from django.db import models

from catalog.models import SubService, Pricing


class Cart(models.Model):
    """
    Represents customer's shopping basket
    """
    # pass
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=255, default="")
    # placed_order = models.

    @classmethod
    def get_cart(cls, cart_id=None):
        """
        Returns existing cart or creates new one
        """
        if cart_id:
            return cls.objects.get(id=cart_id)

        return cls.objects.create()

    def add_item(self, product_id, quantity, price_id, user):
        """
        Add or augment quantity of product
        """
        if self.items.filter(product_id=product_id):
            item = self.items.get(product_id=product_id)
            item.pricing = Pricing.objects.get(id=price_id)
            item.quantity += quantity
            item.save()
            return item

        item = self.items.create(product_id=product_id, pricing=Pricing.objects.get(id=price_id), quantity=quantity,
                                 created_by="")
        return item

    def get_items(self):
        """
        Fetch cart items with products and pics
        """
        return self.items.prefetch_related('product').all()

    def get_items_count(self):
        """
        Returns total number of items
        """
        return len(self.items.prefetch_related('product'))

    def get_subservice_images(self):

        # products = self.items.prefetch_related('product').all()
        # for product in products:
        image = None
        service_pic = self.service_pic.all()
        if len(service_pic) > 0:
            image = service_pic[0].image.url
        else:
            image = settings.DEFAULT_IMAGE
        return image

    def remove_item(self, product_id):
        """
        Remove an item from cart
        """
        try:
            cart_item = self.items.get(product_id=product_id)
            cart_item.delete()
        except CartItem.DoesNotExist:
            pass

    def remove_all_items(self):
        """
        Remove all items form cart
        """
        for item in self.get_items():
            item.delete()

    def get_sub_total(self):
        """
        Sub total of cart items (without taxes)
        """
        sub_total = 0.0
        for item in self.items.all():
            if int(item.pricing.discount) == 0:
                sub_total += int(item.pricing.final_amount)
            else:
                sub_total += int(item.pricing.get_discounted_price)
        return sub_total


class CartItem(models.Model):
    """
    Represents customer's services in basket
    """
    cart = models.ForeignKey(Cart, related_name='items')
    product = models.ForeignKey(SubService)
    pricing = models.ForeignKey(Pricing)
    price_spec = models.CharField(max_length=255, default="")
    quantity = models.IntegerField(default=1)
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.CharField(max_length=255, default="")

    class Meta:
        ordering = ('id',)
        verbose_name_plural = 'Cart Items'
        unique_together = ('cart', 'product',)
